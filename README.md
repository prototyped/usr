# usr

`/usr/include` and `/usr/lib` for Windows x86/x64.

Visual C++ 2017 compiled Windows x86/x64 binaires.


## Included Libraries

This project contains:

name          | version  | license                  | official site
--------------|----------|--------------------------|------------------
asio          | 1.10.6   | Boost Software License   | https://think-async.com/
libcurl       | 7.55.1   | MIT/X derivate license   | https://curl.haxx.se/
gflags        | 2.1.2    | BSD License              | https://github.com/gflags/gflags
gtest         | 1.8.0    | New BSD License          | https://github.com/google/googletest
libmysqlclient| 6.1.5    | GPL License              | http://dev.mysql.com/doc/connector-c/en/index.html
libsodium     | 1.0.13   | ISC License              | https://github.com/jedisct1/libsodium
libuv		  | 1.14.0   | MIT License              | http://libuv.org
lua           | 5.3.4    | MIT License              | http://www.lua.org
msgpack-c     | 1.2.0    | Apache License v2        | https://github.com/msgpack/msgpack-c
openssl       | 1.1.0f   | Apache License           | https://www.openssl.org
protobuf      | 3.3.0    | BSD License              | https://github.com/google/protobuf
rapidjson     | 1.1.0    | MIT License              | https://github.com/miloyip/rapidjson
zeromq        | 4.2.1    | LGPL License v3          | http://www.zeromq.org/
zlib          | 1.2.11   | Zlib License             | http://zlib.net/
